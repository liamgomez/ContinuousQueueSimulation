# QUEUE SCHEDULING SIMULATOR

## About:
This is an implementation of a single server queue that 
implements a few different scheduling policies

## Requirements:
Mongodb, Nodejs, some package manager i.e. npm

## Setup:
1. Make sure mongo and node.js are installed
3. Run npm install in both the _Client_ and _Server_ directories
2. There are two ways to run program:
    - run the shell script _"run.sh [scheduling-name] [number-of-jobs]"_
        - schedule-name (one of the following): fcfs | lcls | sjf | srtf | priority
        - number-of-jobs: must be a positive integer that is greater than 10

    - Run in two seperate terminal windows (good for watching logging from both client and server)
        - start mongodb _"mongod"_
        - in one of the windows run _"node [path-to-client.js] [schedule-name] [simulation-identifier]"_
        - schedule-name (one of the following): fcfs | lcls | sjf | srtf | priority      
        - simulation-identifier: the id of the simulation (string used to save finished tasks in db under)
        - in the other window run _"node [path-to-server.js] [number-of-jobs]"_
        - number-of-jobs: must be a positive integer that is greater than 10

## Future Plans:
- ~~Add socket.io to both projects to alert client side when server has finished processing~~
- Add client side access to reading the mongodb 
- Add client side logic for working with mongodb data (visualization)
