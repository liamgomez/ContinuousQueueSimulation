module.exports = function(app, queue) {
    app.post('/setup', (req, res) => {     
        queue.numberOfJobs = req.body.numberOfJobs;
        queue.totalJobs = req.body.numberOfJobs;
        queue.simulationIdentifier = req.body.runId;

        console.info(`App Setup complete, number of tasks: ${req.body.numberOfJobs}`);

        res.status(200).json({
            'success' : 1
        });
    });
    app.post('/task', (req, res) => {     
        console.info(`Task Recieved: ${JSON.stringify(req.body.task)}`);

        let task = {
            'Id' : Number(req.body.task.Id),
            'ArrivalTime' : null,
            'FinishTime' : null,
            'DelayTime' : null,
            'StartTime' : null,
            'RoundTripTime' : null,
            'RemainingServiceTime' : Number(req.body.task.RemainingServiceTime),            
            'ServiceTime' : Number(req.body.task.ServiceTime)
        };

        queue.AddTask(task);

        res.status(200).json({
            'success' : 1
        });
    });

    app.get('/stats', (req, res) => {
        console.info('Client requested queue statistics');

        queue.GetStats();
        res.status(200).json({
            'Units' : 'milliseconds',
            'AvgServiceTime' : queue.Averages.ServiceTime,
            'AvgDelayTime' : queue.Averages.DelayTime,
            'AvgInterArrival' : queue.Averages.InterArrivalTime,
            'AvgRoudTripTime' : queue.Averages.RoundTripTime,
            'NumberOfJobs' : queue.tasks.length
        });
    });
};