module.exports = function(queue) {
    return queue.tasks.sort((a, b) => {return a.RemainingServiceTime > b.RemainingServiceTime});
}