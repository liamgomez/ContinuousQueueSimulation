class Queue {
    constructor(policy_type='fcfs') {
        switch(policy_type) {
            case 'fcfs':
            default:
                this.sort = null;
                break;
            case 'lcls':
                this.sort = require('../sorting/lcls');
                break;
            case 'sjf':
                this.sort = require('../sorting/sjf');
                break;
            case 'srtf':
                this.sort = require('../sorting/srtf');
                break;
            case 'priority':
                this.sort = require('../sorting/priority');
                break;
        }

        this.policyName = policy_type;
        this.tasks = [];
        this.finishedTasks = [];        
        this.currentTask = null;
        this.timeoutToken = null;
        this.simulationIdentifier;
        this.numberOfJobs;
        this.totalJobs;
        this.queueStartTime = (new Date).getTime();

        this.Averages = {
            ServiceTime : -1,
            InterArrivalTime : -1,
            AverageRoundTripTime : -1,
            AverageDelayTime : -1,
        }     
    }

    AddTask(task) {        
        if (this.timeoutToken !== null) {
            clearInterval(this.timeoutToken);
            this.timeoutToken = null;            
        }

        task.ArrivalTime = (new Date).getTime() - this.queueStartTime;
        this.tasks.push(task);
        this.currentTask = this.tasks[0];

        this.timeoutToken = setInterval(() => { 
            this.Process(this.timeoutToken);
        }, 1);
    }

    Process(cancelationToken=null) {
        this.currentTask.RemainingServiceTime--;

        if (this.currentTask.StartTime === null) {
            this.currentTask.StartTime = (new Date).getTime() - this.queueStartTime;                
        }

        if (this.policyName === 'srtf') {
            this.sort(this);     
        }

        if (this.currentTask.RemainingServiceTime <= 0) {
            this.currentTask.FinishTime = (new Date).getTime() - this.queueStartTime;
            this.currentTask.DelayTime = this.currentTask.StartTime - this.currentTask.ArrivalTime;
            this.currentTask.RoundTripTime = this.currentTask.FinishTime - this.currentTask.ArrivalTime;

            this.tasks.shift();
            this.finishedTasks.push(this.currentTask);

            // require('../Database/DatabaseManager').SaveTask(this.currentTask, this.simulationIdentifier);

            console.info(`Finished processing task with Id: ${this.currentTask.Id}, Arrival time: ${this.currentTask.ArrivalTime}`);

            // var debugTaskStats = {
            //     'ArrivalTime' : this.currentTask.ArrivalTime + "ms",
            //     'FinishTime' : this.currentTask.FinishTime + "ms",
            //     'DelayTime' : this.currentTask.DelayTime + "ms",
            //     'RoundTripTime' : this.currentTask.RoundTripTime + "ms",
            //     'RemainingServiceTime' : this.currentTask.RemainingServiceTime + "ms",
            //     'ServiceTime' : this.currentTask.ServiceTime + "ms",
            // }

            // console.info(`Stats for task ${this.currentTask.Id} : \n${JSON.stringify(debugTaskStats, null, 2)}`);

            clearInterval(cancelationToken);

            if (--this.numberOfJobs <= 0) {
                this.CalculateStats();

                require('../Database/DatabaseManager').SaveTasks(this.finishedTasks, this.simulationIdentifier);

                require('../Database/DatabaseManager').SaveRunStats({
                    'Units' : 'milliseconds',
                    'AvgServiceTime' : this.Averages.ServiceTime,
                    'AvgDelayTime' : this.Averages.DelayTime,
                    'AvgInterArrival' : this.Averages.InterArrivalTime,
                    'AvgRoudTripTime' : this.Averages.RoundTripTime,
                    'NumberOfJobs' : this.totalJobs
                }, this.simulationIdentifier); 
                
                console.log("Simulation Complete");
            }

            if (this.tasks[0]) {
                if (this.sort) {
                    this.sort(this);
                }

                this.currentTask = this.tasks[0];
                this.timeoutToken = setInterval(() => { 
                    this.Process(this.timeoutToken);
                }, 1);
            }
        }
    }

    CalculateStats() {
        var totalServiceTime = 0;
        var totalDelayTime = 0;
        var totalInterarrivalTime = 0;
        var totalRoundTripTime = 0;

        var sortedTasks = this.finishedTasks.sort((a, b) => { return a.ArrivalTime > b.ArrivalTime; });

        for (var i = 0; i < this.finishedTasks.length; ++i) {
            this.finishedTasks[i].RoundTripTime = 
                this.finishedTasks[i].FinishTime - this.finishedTasks[i].ArrivalTime;

            totalServiceTime += this.finishedTasks[i].ServiceTime;
            totalDelayTime += this.finishedTasks[i].DelayTime;
            totalRoundTripTime += this.finishedTasks[i].RoundTripTime;

            if (i < sortedTasks.length - 1)
                totalInterarrivalTime += sortedTasks[i + 1].ArrivalTime - sortedTasks[i].ArrivalTime;
        }

        console.log(JSON.stringify(sortedTasks, null, 2));
        let len = this.finishedTasks.length;
        this.Averages.DelayTime = totalDelayTime / len;
        this.Averages.InterArrivalTime = totalInterarrivalTime / len;
        this.Averages.RoundTripTime = totalRoundTripTime / len;
        this.Averages.ServiceTime = totalServiceTime / len;
    }
}

module.exports.Queue = Queue;