
module.exports = {
    SaveTasks : function(tasks, runId) {
        var mongo = require('mongodb').MongoClient;
        const dbPath = `mongodb://localhost:27017/SchedulingSims`

        mongo.connect(dbPath, (err, db) => {
            if (err)
                db.close();

            db.collection('TaskScheduling').insertOne({
                'runId' : runId,
                'tasks' : tasks
            }, (err, res) => {
                if (err)
                    console.error('Failed to write to database');

                db.close();
            })
        });
    },

    SaveRunStats: function(stats, runId) {
        var mongo = require('mongodb').MongoClient;
        const dbPath = `mongodb://localhost:27017/SchedulingSims`

        mongo.connect(dbPath, (err, db) => {
            if (err)
                db.close();

            db.collection('SimulationResults').insertOne({'runId' : runId, 'results' : stats}, (err, res) => {
                if (err)
                    console.error('Failed to write to database');

                db.close();
            })
        });
    }
}