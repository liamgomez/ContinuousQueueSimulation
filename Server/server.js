const express = require('express');
const bodyParser = require('body-parser');
var queue = require('./app/structures/queue');
const app = express();
const port = 8080;

var Queue = new queue.Queue(process.argv[2]);

// set http body parsing middleware
app.use(bodyParser.json());

// setup and define API routes
require('./app/routes/index')(app, Queue);

// start server 
app.listen(port, () => {
  console.info(`Single Server Queue running on port ${port} with scheduling policy ${Queue.policyName}`);
});