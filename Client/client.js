const express = require('express');
const bodyParser = require('body-parser');
let axios = require('axios');
const app = express();
const port = 3000;

let totalTasks = -1;

// set http body parsing middleware
app.use(bodyParser.json());

// start server 
app.listen(port, () => {
    console.info(`Single Server Queue running on port ${port}`);
    totalTasks = process.argv[2];

    axios.post('http://localhost:8080/setup', {
        runId : process.argv[3],
        numberOfJobs : totalTasks
    })
    .then((res) => {
        GenerateJobs(totalTasks);        
    })
    .catch((err) => {
        console.error(`App initialization failed, ${err}`)
    })
});

GenerateJobs = function(numberOfProcesses, batchInterval = 0, currentTaskNum = 1) {    
    if (numberOfProcesses > 0) {
        console.info(`Sending task ${currentTaskNum} of ${totalTasks}`);

        let serviceTime = Math.ceil(Math.random() * 5000); // 1 to 5,000 millisecs

        axios.post('http://localhost:8080/task', {
            task: {
                'Id' : currentTaskNum,
                'ServiceTime' : serviceTime,
                'RemainingServiceTime' : serviceTime,
                'Priority' : Math.floor(Math.random() * 10)  
            }
        })
        .then((res) => {
            console.info(`Task sent with response: ${JSON.stringify(res.status)}`);
            batchInterval = Math.ceil(Math.random() * 2500); // random wait between posts 1 to 2500 millisecs
            setTimeout(GenerateJobs.bind(null, --numberOfProcesses, batchInterval, ++currentTaskNum), batchInterval);
        })
        .catch((err) => {
            console.error(`Error, task not sent. Status code: ${err}`);
            process.exit(1);
        })
    }
}