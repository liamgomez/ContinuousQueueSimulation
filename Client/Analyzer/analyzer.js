let mongoose = require('mongoose');
let MongoClient = require('mongodb').MongoClient;
let Path = require('path');
mongoose.Promise = global.Promise;

const DB_URL = 'mongodb://localhost:27017/SchedulingSims';

// too lazy to get mongoose running, just gonna old school query 
// let FinishedTask = require('./Models/FinishedTask')();
// let SimStats = require('./Models/SimulationStatistics')();

let runId = process.argv[2];

let GetSimulationResults = function(runId, callback) {
    MongoClient.connect(DB_URL, (err, db) => {
        if (err) return console.log(err);

        db.collection('SimulationResults').findOne({'runId' : runId}, (err, sims) => {
            if (err) {
                console.log(err);
                return;
            }

            let id = runId;
            let res = {
                'sim' : sims
            };


            db.collection('TaskScheduling').findOne({'runId' : id}, (error, tasks) => {
                if (error) {
                    console.log(error);
                    return;
                }
    
                db.close();
                res['tasks'] = tasks;

                callback(null, res);
            });
        });
    });
}

GetSimulationResults(runId, (err, res) => {
    // console.log(JSON.stringify(res, null, 2));

    // this is where we will output statistics that we may want 
    
});

