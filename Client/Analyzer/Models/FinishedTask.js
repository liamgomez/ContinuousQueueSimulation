let mongoose = require('mongoose');
let Schema = mongoose.Schema;

module.exports = function() {
    let FinishedTask = new Schema({
        _id:  String,
        runId: String,
        tasks: [{
            Arrival: Number,
            Finish:   Number,
            RoundTrip: Number,
            Delay: Number,
            Service: Number,
            RemainingService: Number
        }]
      }, {
          collection: 'TaskScheduling'
      });
      
      return mongoose.model("FinishedTask", FinishedTask);
}
