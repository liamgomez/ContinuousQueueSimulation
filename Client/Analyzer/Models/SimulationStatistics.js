let mongoose = require('mongoose');
let Schema = mongoose.Schema;

module.exports = function() {
    let SimStats = new Schema({
        _id:  String,
        runId: String,
        results: [{
            RunIdentifier: String,
            Units:   Number,
            AvgServiceTime: Number,
            AvgDelayTime: Number,
            AvgInterArrival: Number,
            AvgRoudTripTime: Number,
            NumberOfJobs: Number
        }]
      }, {
          collection: 'SimulationResults'
      });
      
      return mongoose.model("SimulationStatistics", SimStats);
}
