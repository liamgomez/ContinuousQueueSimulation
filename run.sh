if [ "$1" == "" ] ; then
    echo "Error: you need to specify the scheduling policy."; >&2; exit 1

fi

if [ "$1" != "fcfs" ] || [ "$1" != "lcls" ] || [ "$1" != "sjf" ] || [ "$1" != "srtf" ] || [ "$1" != "priority" ] ; then
    echo "Invalid policy provided, options are: fcfs, lcls, sjf, srtf, or priority"; >&2; exit 1
fi

# r.e. to check for number
re='^[0-9]+$'
if ! [[ $2 =~ $re ]] ; then
   echo "Error: Arguement 2 must be a number" >&2; exit 1
elif [ "$2" -lt "10" ] ; then
    echo "Error: Arguement 2 must be greater than or equal to 10" >&2; exit 1
fi

echo "\n\nLaunching mongo db server"
mongod

if [ $? -eq 0 ] ; then
    echo "Mongo db failed to start" >&2; exit 1
fi


echo "\n\nStarting Single queue server now with policy $1\n\n"
node Server/server.js $1 &

# give the server time to start
sleep 3

echo "Server now running...\n\n"
echo "Starting client side, scheduled to batch $2 tasks\n\n"
node Client/client.js $2
